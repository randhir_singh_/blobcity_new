var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
 var cons = require('consolidate'); //for html view engine
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//var index = require('./routes/index');
//var users = require('./routes/users');
var userLogin = require('./routes/v1/user/user-login');
var userRegister = require('./routes/v1/user/user-register');
var userVerifyEmail = require('./routes/v1/user/verify-email');
var userRequestNewPassword = require('./routes/v1/user/request-new-password');
var userChangePassword = require('./routes/v1/user/change-password');
var userResetPassword = require('./routes/v1/user/reset-password');
var userDeleteAll = require('./routes/v1/user/delete-all');
var appsHome = require('./routes/v1/apps/apps');
var registerCard = require('./routes/v1/billing/register-card');
var session = require('./routes/v1/user/session');
//<<<<<<< HEAD
var accountHome = require('./routes/v1/account/home');

/* Typeforms */
var dbRegistrationTypeform = require('./routes/v1/typeform/db-form-submission');
//=======
var logout = require('./routes/v1/user/logout');
var cards = require('./routes/v1/user/cards');
var addCard = require('./routes/v1/user/add-card');
var setCardPrimary = require('./routes/v1/user/set-card-primary');
var deleteCard = require('./routes/v1/user/delete-card');

/* Datastore level operations */
var setDs = require('./routes/v1/db/set-ds');
var setFtpOn = require('./routes/v1/db/set-ftp-on');
var setFtpOff = require('./routes/v1/db/set-ftp-off');
var getFtpPass = require('./routes/v1/db/get-ftp-pass');
var resetFtpPass = require('./routes/v1/db/reset-ftp-pass');

/* Account level operations */
var accountHome = require('./routes/v1/account/home');
//>>>>>>> fb087edfd024b03d13435a610d33124d168388c5

/* BlobCity Academy components */
var academyEmailCapture = require('./routes/academy/email-capture');
var newCourseEnrollment = require('./routes/academy/new-enrollment');

var app = express();

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');
app.engine('html', cons.swig);
app.set('view engine', 'html');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', index);
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname,'public/views/typeform.html'));
});
/* User paths */
//app.use('/users', users);
app.use('/rest/v1/user/login', userLogin);
app.use('/rest/v1/user/register', userRegister);
app.use('/rest/v1/user/verify-email', userVerifyEmail);
app.use('/rest/v1/user/request-new-password', userRequestNewPassword);
app.use('/rest/v1/user/change-password', userChangePassword);
app.use('/rest/v1/user/reset-password', userResetPassword);
app.use('/rest/v1/user/register-card', registerCard);
app.use('/rest/v1/user/session', session);
app.use('/rest/v1/user/logout', logout);
app.use('/rest/v1/user/cards', cards);
app.use('/rest/v1/user/add-card', addCard);
app.use('/rest/v1/user/set-card-primary', setCardPrimary);
app.use('/rest/v1/user/delete-card', deleteCard);

/* Datastore paths */
app.use('/rest/v1/db/set-ds', setDs);
app.use('rest/v1/db/set-ftp-on', setFtpOn);
app.use('/rest/v1/db/set-ftp-off', setFtpOff);
app.use('/rest/v1/db/get-ftp-pass', getFtpPass);
app.use('/rest/v1/db/reset-ftp-pass', resetFtpPass);

/* Account paths */
app.use('/rest/v1/account/home', accountHome);

/* Typeform Webhooks */
app.use('/rest/v1/typeform/db-form-submission', dbRegistrationTypeform);

/* Academy paths */
app.use('/academy/email-capture', academyEmailCapture);
app.use('/academy/new-enrollment', newCourseEnrollment);

/* Apps Path */
app.use('/rest/apps', appsHome);

/* Register this only if deployed on Sandbox */
app.use('/rest/user/delete-all', userDeleteAll);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.post('/academy/grab-email', function(req, res) {
    var name = req.body.FNAME;
    var email = requ.body.EMAIL;

    console.output(name + ' ' + email);
});



module.exports = app;
