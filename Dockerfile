FROM node:7.8-alpine

WORKDIR /src

COPY package.json /src/package.json

# Bundle app source
COPY . /src

RUN ["npm", "install]

EXPOSE  8080
EXPOSE 3000

WORKDIR /src

CMD ["npm", "start"]
