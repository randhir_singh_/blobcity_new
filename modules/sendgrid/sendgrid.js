

exports.sendmail = function(templateId, to, subject, body) {
    var credentials = require('../../bin/credentials');
    var sg = require('sendgrid')(credentials.sendgrid);

    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: {
            template_id: templateId,
            personalizations: [
                {
                    to: to
                }
            ],
            from: {
                email: 'contact@blobcity.xyz',
                name: 'BlobCity'
            },
            reply_to: {
                email: 'contact@blobcity.xyz',
                name: ' BlobCity'
            },
            content: [
                {
                    type: 'text/plain',
                    value: body
                },
                {
                    type: 'text/html',
                    value: body
                }
            ]
        }
    });

    sg.API(request, function (error, response) {
        if (error) {
            console.log('Error in sendgrid.sendmail(templateId, to, body)');
            console.log(error);
        }
    });
}