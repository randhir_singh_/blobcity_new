

exports.scheduleEmail = function(mailName, to, from, params, sendAfter) {
    db.sql('select * from `portal`.`BAEmails` where `mailName`=\'' + mailName + '\'', function(result) {
        if(db.isSuccess(result) && db.data(result).length == 1) {
            var email = db.data(result)[0];

            var textBody = email.textBody;
            var htmlBody = email.htmlBody;

            for(var param in params) {
                textBody = textBody.replace('{{' + param + '}}', params[param]);
                htmlBody = htmlBody.replace('{{' + param + '}}', params[param]);
            }

            var emailData = {
                to: to,
                from: from,
                textBody: textBody,
                htmlBody: htmlBody,
                sendAfter: sendAfter
            };

            db.insert('BAScheduledEmails', [emailData], function(result) {
                if(db.isSuccess(result)) {
                    console.log('Scheduled email ' + JSON.stringify(emailData));
                } else {
                    console.error('Failed to schedule email ' + JSON.stringify(emailData));
                }
            });

        } else if(db.isSuccess(result) && db.data(result).length > 1) {
            console.error('Multiple email templates found for name = ' + mailName + '. No email is scheduled');
        } else {
            console.error('No email template found for name ' + mailName);
        }
    });
}