
var db = require('../logic/db/blobcity').db();
var credentials = require('../bin/credentials');

module.exports = {
    userApps: function(uid, callback) {userApps(uid, callback)},
    unsubscribedHostedDatasets: function(uid) {unsubscribedHostedDatasets(uid)},
    unsubcribedHostedDashboards: function(uid) {unsubscribedHostedDashboards(uid)},
    populateAppsDb : function() {populateAppsDb()},
    populatePricingPlans : function (){populatePricingPlans()},
    setDefaultAppMemberships : function(uid){setDefaultAppMemberships(uid)},
    getPricingPlan : function(appId, displayOrder) {getPricingPlan(appId, displayOrder)}
}

var userApps = function(uid, callback) {
    db.sql('select * from `' + credentials.ds + '`.`UserApps` where `uid`=\'' + uid + '\'', function(result) {
        if(db.isSuccess(result)) {
            callback(null, db.data(result));
        } else {
            callback('error');
        }
    });
}

/**
 * Gets the list of hosted datasets the user is not currently subscribed to
 * @param uid user id of the user
 * @returns {Array} the list of datasets
 */
var unsubscribedHostedDatasets = function(uid) {
    var unsubscribedDatasets = [];

    var allDatasets = allHostedDatasets();
    var subscribed = userApps(uid);

    for(var dataset in allDatasets) {
        if(subscribed.filter(x => x.id == dataset.id).length == 0) {
            unsubscribedDatasets.push(dataset);
        }
    }

    return unsubscribedDatasets;
}

/**
 * Gets the list of hosted dashboards the user is not currently subscribed to
 * @param uid user id of the user
 * @returns {Array} the list of dashboards
 */
var unsubscribedHostedDashboards = function(uid) {
    var unsubscribedDashboards = [];

    var allDashboards = allHostedDashboards();
    var subscribed = userApps(uid);

    for(var dashboard in allDashboards) {
        if(subscribed.filter(x => x.id == dashboard.id).length == 0) {
            unsubscribedDashboards.push(dashboard);
        }
    }

    return unsubscribedDashboards;
}

/**
 * Gets the complete list of hosted dashboards
 * @returns {Array} list of dashboards
 */
var allHostedDashboards = function() {
    db.sql('select * from `' + credentials.ds + '`.`Apps` where `type`=`DASHBOARD`', function(result){
        if(db.isSuccess(result)) {
            return db.data(result);
        }
    });

    return [];
}

/**
 * Gets the complete list of hosted datasets
 * @returns {Array} list of datasets
 */
var allHostedDatasets = function() {
    db.sql('select * from `' + credentials.ds + '`.`Apps` where `type`=`DATASET`', function(result){
        if(db.isSuccess(result)) {
            return db.data(result);
        }
    });

    return [];
}

/**
 * Adds entry into the UserApps table for all apps inside the Apps table that are marked with default = true.
 * The pricing plan chosen for each of the apps is the one marked with display-order = 1 for the corresponding app.
 * A app marked as default = true inside Apps table is not added to the UserApps table if a valid pricing plan is not
 * found for the app inside the PricingPlans table
 * @param uid the user id of the user
 */
var setDefaultAppMemberships = function(uid) {
    db.sql('select * from `' + credentials.ds + '`.`UserApps` where `uid`=\'' + uid + '\'', function(result) {
       if(db.isSuccess(result) && db.data(result).isEmpty()) {
           db.sql('select * from `' + credentials.ds + '`.`Apps` where `default`=true', function(result) {
               if(db.isSuccess(result)) {
                   var apps = db.data(result);

                   for(var app in apps) {
                       var pricingPlan = getPricingPlan(app.id, 1);

                       if(pricingPlan == null) {
                           continue;
                       }

                       var userApp = {
                           uid:  uid,
                           appId: app.id,
                           planId: pricingPlan.id
                       };

                       db.insert('UserApps', userApp, function(result){});
                   }
               }
           })
       }
    });
}

/**
 * Populates the Apps table with default values for the apps. This should only be used for addition of statically
 * programmed apps listed inside this function at first time system boot. The function is provided for convenience
 * purpose only and should be used with care.
 */
var populateAppsDb = function() {
    var apps = [
        {
            'id': 'dbmt',
            'name': 'DBMT',
            'type': 'APP',
            'description': 'The database management tool',
            'icon': 'public/images/app-icons/dbmt.png',
            'url': 'http://blobcity.com/apps-transfers/dbmt?st=',
            'public': true,
            'optional': false,
            'default': true
        },
        {
            'id': 'dashboards',
            'name': 'Dashboards',
            'type': 'APP',
            'description': 'Visualise data stored inside BlobCity by creating and sharing dashboards',
            'icon': 'public/images/app-icons/dashboards.png',
            'url': 'http://blobcity.com/apps-transfers/dashboards?st=',
            'public': true,
            'optional': false,
            'default': true
        }
    ];

    var hostedDashboards = [
        {
            'id': 'dashboard1',
            'name': 'Dashboard 1',
            'type': 'DASHBOARD',
            'description': 'Dashboard 1 description',
            'icon': 'public/images/app-icons/dashboard1.png',
            'url': 'http://blobcity.com/apps-transfers/dashboard1?st=',
            'public': true,
            'optional': true,
            'default' : false
        }
    ];

    var hostedDatasets = [
        {
            'id': 'dataset1',
            'name': 'Dataset 1',
            'type': 'DATASET',
            'description': 'Dataset 1 description',
            'icon': 'public/images/app-icons/dataset1.png',
            'url': 'http://blobcity.com/apps-transfers/dataset1?st=',
            'public': true,
            'optional': true,
            'default': false
        }
    ];

    for(var app in apps) {
        db.sql('select * from `' + credentials.ds + '`.`Apps` where `name`=\'' + app.name + '\'', function(err,result){
            if(err) return;

            if(db.data(result).length == 0) {
                db.insert('Apps', app, function(err, result){});
            }
        });
    }

    for(var dashboard in hostedDashboards) {
        db.sql('select * from `' + credentials.ds + '`.`Apps` where `name`=\'' + dashboard.name + '\'', function(err,result){
            if(err) return;

            if(db.data(result).length == 0) {
                db.insert('Apps', dashboard, function(err, result){});
            }
        });
    }

    for(var dataset in hostedDatasets) {
        db.sql('select * from `' + credentials.ds + '`.`Apps` where `name`=\'' + dataset.name + '\'', function(err,result){
            if(err) return;

            if(db.data(result).length == 0) {
                db.insert('Apps', dataset, function(err, result){});
            }
        });
    }
}

/**
 * Used to populate default pricing plans inside the PricingPlans table. This should only be used for populating the
 * PricingPlans table with default plan values that are statically programmed inside this function. The function
 * is provided for convenience for creating default plans at first time boot and should be used with care.
 */
var populatePricingPlans = function() {
    var pricingPlans = [
        {
            'id': 'dbmt-storage-1',
            'app-id': 'dbmt',
            'price': {
                'USD': 0.0139
            },
            'type': 'hourly',
            'extend': 'storage',
            'storage': 1,
            'users': 1,
            'quantity-selectable': false,
            'text': {
                'USD': '$0.0139/GB/hr'
            },
            'display-order': 1,
            'active': true
        },
        {
            'id': 'ds-users-1',
            'app-id': 'ds-users',
            'price': {
                'USD': 5
            },
            'type': 'monthly',
            'extend': 'users',
            'users': 1,
            'quantity-selectable': true,
            'text': {
                'USD': '$5/user/month'
            },
            'display-order': 1,
            'active': true
        },
        {
            'id': 'ds-users-2',
            'app-id': 'users',
            'price': {
                'USD': 50
            },
            'type': 'yearly',
            'extend': 'users',
            'users': 1,
            'quantity-selectable': true,
            'text': {
                'USD': '$50/user/yr'
            },
            'display-order': 2,
            'active': true
        }
    ];

    for(var pricingPlan in pricingPlans) {
        if(db.insert('PricingPlans', pricingPlan, function(result) {
                //do nothing
            }));
    }
}

var getPricingPlan = function(appId, displayOrder) {
    db.sql('select * from `' + credentials.ds + '`.`PricingPlans` where `id`=\'' + appId + '\' and `display-order`=' + displayOrder, function(err,result) {
        if(err) return [];

        if(db.data(result).length == 1) {
            return db.data(result)[0];
        }
    });

    return null;
}