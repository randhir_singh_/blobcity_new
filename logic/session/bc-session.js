var db = require('../db/blobcity').db();
var credentials = require('../../bin/credentials');

module.exports = {
    isValid : function(st) {
        db.sql('select * from `'+ credentials.ds +'`.`UserSession` where `token`=\'' + st + '\'', function(err, result){
            if(err) {
                return false;
            }

            if(db.data(result).length == 1) {
                return true;
            }
        });

        return false;
    },

    inSession : function(st, callback){inSession(st, callback)}
}

var inSession = function(st, callback) {
    db.sql('select * from `' + credentials.ds + '`.`UserSession` where `token`=\'' + st + '\'', function (err, result) {
        if(err || db.data(result).length != 1) {
            callback('no session found');
        } else {
            console.log('user session found: ' + JSON.stringify(db.data(result)[0]));
            callback(null, db.data(result)[0]);
        }
    });
}