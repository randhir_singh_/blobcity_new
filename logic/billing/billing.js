/**
 * Created by sanketsarang
 */
var express = require('express');
var db = require('blobcity');
var session = require('/logic/session/bc-session');
var dbAuth = require('/logic/db/blobcity');
var router = express.Router();

module.exports = {
    getBillingCurrency : function(uid) {
        return getBillingCurrency(uid);
    }
}

/**
 * Returns the billing currency associated with the users account. Typically uses the currency of the billing country
 * if the local currency of that country is supported, else defaults to US Dollars.
 * @param uid the user id of the user
 * @returns {string} the three digit currency code. USD is default.
 */
var getBillingCurrency = function(uid) {
    return 'USD'; //only this is available for the moment.
}