
var http = require('http'); //to remove
var request = require('request');

var username = 'apiuser';
var password = ''; //pick this up from deployment parameters

module.exports = {
    createSite: function(siteName, callback) {
        login('test', function(err, token) {
            if(err) callback(err);
            else {
                createSite(siteName, token, callback);
            }
        })
    }, 

    addUserToSite: function(user, site, callback) {
        login(site, function(err, token) {
            if(err) callback(err);
            else addUserToSite(user, site, token, callback);
        });
    },

    addApiUserToSite: function(site, callback) {
        login(site, function(err, token) {
            if(err) callback(err);
            else addApiUserToSite('apiuser', site, token, callback);
        });
    }
}

var login = function(site, callback) {
    var body = {
        'credentials': {
            'name': username,
            'password': password,
            'site': {
                'contentUrl': site
            }
        }
    };

    var options = {
        headers: {
            'Content-Type': 'application/json'
        },
        uri: 'https://visual.blobcity.com/api/2.6/auth/signin',
        method: 'POST',
        json: body
    };

    request(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            callback(null, body.credentials.token);
        } else {
            callback(error);
        }
    });
}

var createSite = function(siteName, token, callback) {
    var body = {
        'site': {
            'name': siteName,
            'contentUrl': siteName,
            'adminMode': 'ContentOnly'
        }
    };

    authRequest('/sites', body, token, callback);
}

var addUserToSite = function(username, site, token, callback) {
    var body = {
        user: {
            name: username,
            siteRole: 'Publisher'
        }
    }

    authRequest('/sites/' + site + '/users', body, token, callback);
}

var addApiUserToSite = function(username, site, token, callback) {
    var body = {
        user: {
            name: username,
            siteRole: 'Site Administrator'
        }
    }

    authRequest('/sites/' + site + '/users', body, token, callback);
}

var authRequest = function(endpoint, body, token, callback) {
    var uri = 'https://visual.blobcity.com/api/2.6' + endpoint;
    var options = {
        headers: {
            'Content-Type': 'application/json',
            'X-Tableau-Auth': token
        },
        uri: uri,
        method: 'POST',
        json: body
    };

    request(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            callback(null, body);
        } else {
            callback(error);
        }
    });
}