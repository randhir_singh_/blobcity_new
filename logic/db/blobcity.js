var db = require('blobcity');
var credentials = require('../../bin/credentials')

module.exports = {
    db : function() {
        db.auth(credentials.server, credentials.ds, credentials.user, credentials.password);
        return db;
    }
}

