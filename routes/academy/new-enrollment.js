/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var fs = require('fs');
// const nodemailer = require('nodemailer');
var router = express.Router();


/* GET users listing. */
router.post('/', function(req, res, next) {
    // console.log(req.body);

    var data = req.body;

    console.log('course id: ' + data.object.course_id);
    console.log('name: ' + data.object.user.name);
    console.log('name: ' + data.object.user.email);

    switch(data.object.course_id) {
        case 143479:
            // scheduleBUMailOnFreeEnrollment(data.object.user.name, data.object.user.email, 'Big Data Preview course');
            // scheduleGenericBUMail(data.object.user.name, data.object.user.email);
            break;
        case 144954:
            // sendBUMail(data.object.user.name, data.object.user.email, 'AI & ML Webinar');
            break;
    }

    res.send('');
});

var scheduleBUMailOnFreeEnrollment = function(name, to, courseName) {

}

var scheduleFeedbackEmail = function(mailName, userName, to) {
    db.sql('select * from `portal`.`BAEmails` where `mailName`=\'' + mailName + '\'', function(result) {
        if(db.isSuccess(result) && db.data(result).length == 1) {
            var email = db.data(result)[0];

            var textBody = email.textBody;
            var htmlBody = email.htmlBody;

            textBody = textBody.replace('{{name}}', userName);
            htmlBody = htmlBody.replace('{{name}}', userName);

            var sendAfter = Date.now() + 60*60*1000; //60 min later

            var emailData = {
                to: to,
                from: 'BlobCity Academy<noreply@blobcity.com>',
                textBody: textBody,
                htmlBody: htmlBody,
                sendAfter: sendAfter
            };

            db.insert('BAScheduledEmails', [emailData], function(result) {
                if(db.isSuccess(result)) {
                    console.log('Scheduled email ' + JSON.stringify(emailData));
                } else {
                    console.error('Failed to schedule email ' + JSON.stringify(emailData));
                }
            });

        } else if(db.isSuccess(result) && db.data(result).length > 1) {
            console.error('Multiple email templates found for name = ' + mailName + '. No email is scheduled');
        } else {
            console.error('No email template found for name ' + mailName);
        }
    });
}

var scheduleGenericBUMail = function(name, to) {
    db.sql('select * from `portal`.`BAEmails` where `mailName`=\'BU_GENERIC\'', function(result){
        if(db.isSuccess(result) && db.data(result).length == 1) {
            var email = db.data(result)[0];
        } else if(db.isSuccess(result) && db.data(result).length > 1) {
            console.error('Multiple email templates found for name = BU_GENERIC. No email is scheduled');
        } else {
            console.error('No email template found for name BU_COURSE_SPECIFIC');
        }
    });
}

// var sendBUMail = function(name, to, courseName) {
//
//     // var from = 'BlobCity Academy <noreply@blobcity.com>';
//     // var subject = 'BlobCity Academy: Would you like to take all courses free for 30 days?';
//     // var body = 'Hi ' +  name + "\n\n"
//     //     + 'Now that you are watching the ' + courseName + ', would you like to watch all our courses free for 30 days?\n\n'
//     //     + 'Use the below link to get a 30 days no obligation trial of BlobCity Unlimited.\n\n'
//     //     + 'http://learn.blobcity.com/courses/blobcity-unlimited?product_id=247435&coupon_code=BU30OFF\n\n'
//     //     + 'BlobCity Unlimited gives you access to all courses within our academy for a flat monthly fee. The first month is on us. You may cancel your subscription at anytime and would not be billed for the following months.\n\n'
//     //     + 'Happy Learning!\n'
//     //     + 'BlobCity Team\n'
//     //     + 'http://learn.blobcity.com';
//
//     // create reusable transporter object using the default SMTP transport
//     let transporter = nodemailer.createTransport({
//         service: 'gmail',
//         auth: {
//             user: 'noreply@blobcity.com',
//             pass: 'BlobCity@123'
//         }
//     });
//
//     // setup email data with unicode symbols
//     let mailOptions = {
//         from: 'BlobCity Academy <noreply@blobcity.com>', // sender address
//         to: to, // list of receivers
//         subject: 'Would you like access to the entire academy?', // Subject line
//         text: 'Hi ' + name + '\n\n'
//         + 'Now that you are watching the ' + courseName + ', would you like to watch all our courses free for 30 days?\n\n'
//         + 'Use the below link to get a 30 days no obligation trial of BlobCity Unlimited.\n\n'
//         + 'http://bit.ly/2lFSDRT\n\n'
//         + 'BlobCity Unlimited gives you access to all courses within our academy for a flat monthly fee. The first month is on us :)\n\n'
//         + 'Happy Learning!\n'
//         + 'BlobCity Team\n'
//         + 'http://learn.blobcity.com',
//         html: '<div width: 100%"><span style="font-size: xx-large"><span style="color: #ff8c00">Blob</span><span style="color: #000">City</span><span style="color: #333"> Academy</span></span></div>'
//         + '<br/><hr/><br/>'
//         + 'Hi ' + name + '<br/><br/>'
//         + 'Now that you are watching the ' + courseName + ', would you like to watch all our courses free for 30 days?<br/><br/>'
//         + 'Use the below link to get a 30 days no obligation trial of BlobCity Unlimited.<br/><br/>'
//         + '<a href=\'http://bit.ly/2lFSDRT\'>Start your 30 day trial</a><br/><br/>'
//         + 'BlobCity Unlimited gives you access to all courses within our academy for a flat monthly fee. The first month is on us :)<br/><br/>'
//         + 'Happy Learning!<br/>'
//         + 'BlobCity Team<br/>'
//         + 'http://learn.blobcity.com'
//     };
//
//     // send mail with defined transport object
//     transporter.sendMail(mailOptions, (error, info) => {
//         if (error) {
//             return console.log(error);
//         }
//         console.log('Message %s sent: %s', info.messageId, info.response);
// });
// }

module.exports = router;
