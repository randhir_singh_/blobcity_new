/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('blobcity');
var crypto = require('crypto');
var router = express.Router();
var uuidV4 = require('uuid.v4');
var credentials = require('../../../bin/credentials');


/* GET users listing. */
router.get('/', function(req, res, next) {
    var st = req.query.st;
    var stripe_token = req.query.stripe_token;

    /* Set DB auth credentials */
    db.auth(credentials.server, credentials.ds, credentials.user, credentials.password);

    db.sql('select * from `' + credentials.ds + '`.`SessionTokens` where `st`=\'' + st + '\'', function (err, result) {
        if(err) {
            res.send(JSON.stringify({ack: '0', error: 'invalid session'}));
        } else {
            if(result == null || result.length == 0) {
                res.send(JSON.stringify({ack: '0', error: 'internal'}));
            } else {
                var uid = result[0].uid;
                db.sql('select * from `' + credentials.ds + '`.`StripeCards` where `token`=\'' + stripe_token + '\'', function(err, result) {
                    if(err) {
                        res.send(JSON.stringify({ack: '0', error: 'internal'}));
                    } else if (result.size > 0) {
                        //do nothing as card is already registered. Simply return success
                        res.send(JSON.stringify({ack: '1'}));
                    } else {
                        db.insert('StripeCards', {'uid': uid, 'token': stripe_token}, function(err, result) {
                            if(err) {
                                res.send(JSON.stringify({ack: '0', error: 'internal'}));
                            } else {
                                res.send(JSON.stringify({ack: '1'}));
                            }
                        });
                    }
                });
            }
        }
    });
});

module.exports = router;
