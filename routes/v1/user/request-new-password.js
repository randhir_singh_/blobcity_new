/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var uuidV4 = require('uuid.v4');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
    var email = req.query.email;

    /* If email is associated with an account, then send a reset password email */

    db.sql('select * from `portal`.`User` where `email`=\'' + email + '\'', function(err, result) {
        if(err) {
            res.send('failure');
            return;
        }

        if(db.data(result).length == 1) {
            res.send('success');
            initiatePasswordReset(email);
        } else {
            res.send('failure');
        }
    })
});

var initiatePasswordReset = function(email) {
    var token = uuidV4();
    db.insert('PasswordResetTokens', [{email: email, token: token}], function(err, result) {
        if(err) {
            return;
        }

        //TODO: UNCOMMENT FOR PRODUCTION
        sendmail(email, token);
    });
}

var sendmail = function(email, token) {
    console.log('new password request email');
}

// var sendmail = function(email, token) {
//     // create reusable transporter object using the default SMTP transport
//     let transporter = nodemailer.createTransport({
//         service: 'gmail',
//         auth: {
//             user: 'noreply@blobcity.com',
//             pass: 'BlobCity@123'
//         }
//     });
//
//     // setup email data with unicode symbols
//     let mailOptions = {
//         from: 'BlobCity <noreply@blobcity.com>', // sender address
//         to: email, // list of receivers
//         subject: 'BlobCity: Account password reset instructions', // Subject line
//         text: 'Visit the link below to set a new password to your account. The link is valid for only 15 minutes. \n\n'
//         + 'http://blobcity.com/password-reset?token=' + token + '\n\n'
//         + 'If you did not initiate this request then immediately contact us at support@blobcity.com as someone maybe trying to hack your account.\n\n'
//         + 'Regards,\n'
//         + 'BlobCity Team\n'
//         + 'http://blobcity.com'
//     };
//
//     // send mail with defined transport object
//     transporter.sendMail(mailOptions, (error, info) => {
//         if (error) {
//             return console.log(error);
//         }
//         console.log('Message %s sent: %s', info.messageId, info.response);
//     });
// }

module.exports = router;
