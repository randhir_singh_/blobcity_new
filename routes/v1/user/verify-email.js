/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var credentials = require('../../../bin/credentials');
var router = express.Router();

router.get('/', function(req, res, next) {
    var token = req.query.token;

    handelRequest(token, res);
});

router.post('/', function(req, res, next) {
    var token = req.body.token;

    handelRequest(token, res);
});

module.exports = router;

var handelRequest = function(token, res) {
    db.sql('select * from `' + credentials.ds + '`.`VerifyEmail` where `token`=\'' + token + '\'', function(err, result) {
        if(err) {
            res.send('failure');
            return;
        }

        if(db.data(result).length == 1) {
            var email = db.data(result)[0].email;

            /* Set user to verified */
            db.sql('update `' + credentials.ds + '`.`User` set `verified`=true where `email`=\'' + email + '\'', function(err, result) {
                if(err) {
                    res.send('failure');
                    return;
                }

                res.send('success');
            });
        } else {
            res.send('failure');
        }
    });
};
