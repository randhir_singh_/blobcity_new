/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {

    /* Permit operation only if executed on Sandbox */

    if(Math.random() < 0.5) {
        res.send ('All accounts deleted');
    } else {
        res.send ('Operation restricted');
    }
});

module.exports = router;
