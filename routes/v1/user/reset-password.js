/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var credentials = require('../../../bin/credentials');
var crypto = require('crypto');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
    var token = req.query.token;
    var password = req.query.password;

    /* If token is valid, set the password of the account to one passed */

    db.sql('select * from `' + credentials.ds + '`.`PasswordResetTokens` where `token`=\'' + token + '\'', function(err, result) {
       if(err) {
           res.send('failure');
           return;
       }

        var data = db.data(result);
        if(data.length == 1) {
            var email = data[0].email;
            if(updatePassword(email, password)) {
                console.log('Password for user ' + email + ' successfully updated');
                res.send('success');
            }else {
                console.log('Password updation for user ' + email + ' failed');
                res.send('failure');
            }
        } else {
            res.send('failure');
        }
    });
});

var updatePassword = function(email, password) {
    var passwordHash = crypto.createHash('md5').update(password).digest("hex");
    db.sql('update `' + credentials.ds + '`.`User` set `password`=\'' + passwordHash + '\' where `email`=\'' + email + '\'', function(result) {
        return db.isSuccess(result);
    });

    return true;
}

module.exports = router;
