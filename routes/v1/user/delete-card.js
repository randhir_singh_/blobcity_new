/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var credentials = require('../../../bin/credentials');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    var st = req.query.st;
    var id = req.query.id;

    handelRequest(st, id, res);
});

router.post('/', function(req, res, next) {
    var st = req.query.st;
    var id = req.query.id;

   handelRequest(st, id, res);
});

module.exports = router;

var handelRequest = function(st, id, res) {
    db.sql('select * from `' + credentials.ds + '`.`UserSession` where `token`=\'' + st + '\'', function(err, result) {
        if(err) {
            res.send (JSON.stringify({ack: '0'}));
            return;
        }

        var data = db.data(result);
        if(data.length == 1) {
            var uid = data[0].uid;

            var sql = 'delete from `' + credentials.ds + '`.`StripeCards` where `id`=\'' + id + '\' and `uid`=\'' + uid + '\'';
            console.log(sql);
            db.sql(sql, function(err, result) {
               if(err) {
                   res.send(JSON.stringify({ack: '0'}));
                   return;
               }

               res.send(JSON.stringify({ack: '1'}));
            });
        } else {
            res.send (JSON.stringify({ack: '0'}));
        }
    });
}