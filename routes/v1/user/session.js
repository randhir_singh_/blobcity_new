/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var credentials = require('../../../bin/credentials');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    var token = req.query.st;

    handelRequest(token,res);
});

router.post('/', function(req, res, next) {
   var token = req.body.st;

   handelRequest(token, res);
});

module.exports = router;

var handelRequest = function(token, res) {
    db.sql('select * from `' + credentials.ds + '`.`UserSession` where `token`=\'' + token + '\'', function(err, result) {
        if(err) {
            res.send (JSON.stringify({ack: '0'}));
            return;
        }

        var data = db.data(result);
        if(data.length == 1) {
            var session = data[0];

            db.sql('select * from `' + credentials.ds + '`.`User` where `uid`=\'' + session.uid + '\'', function(err, userResult) {
                if(err) {
                    res.send (JSON.stringify({ack: '0'}));
                    return;
                }

                if(db.data(userResult).length == 1) {
                    var user = db.data(userResult)[0];
                    var responseJson = {
                        ack: '1',
                        name: user.name,
                        email: user.email
                    };
                    res.send(JSON.stringify(responseJson));
                } else {
                    res.send (JSON.stringify({ack: '0'}));
                }
            });
        } else {
            res.send (JSON.stringify({ack: '0'}));
        }
    });
}