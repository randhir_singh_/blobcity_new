/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var crypto = require('crypto');
var router = express.Router();
var credentials = require('../../../bin/credentials');
//var pwdGenerator = require('generate-password');
var sendgrid = require('../../../modules/sendgrid/sendgrid');
var generatePassword = require("password-generator");
var uuidV4 = require('uuid.v4');


/* GET users listing. */
router.get('/', function(req, res, next) {
    var name = req.query.name;
    var email = req.query.email;
    var password = req.query.password;

    handelRequest(name, email, password, res);
});

router.post('/', function(req, res, next) {
   var name = req.body.name;
   var email = req.body.email;
   //var password = Math.random().toString(36).slice(-8); //autogenerate Passwod
   ///////////////////////////////Genearte Password
var maxLength = 8;
var minLength = 8;
var uppercaseMinCount = 2;
var lowercaseMinCount = 2;
var numberMinCount = 2;
var specialMinCount = 2;
var UPPERCASE_RE = /([A-Z])/g;
var LOWERCASE_RE = /([a-z])/g;
var NUMBER_RE = /([\d])/g;
var SPECIAL_CHAR_RE = /([\?\-])/g;
var NON_REPEATING_CHAR_RE = /([\w\d\?\-])\1{2,}/g;

function isStrongEnough(password) {
  var uc = password.match(UPPERCASE_RE);
  var lc = password.match(LOWERCASE_RE);
  var n = password.match(NUMBER_RE);
  var sc = password.match(SPECIAL_CHAR_RE);
  var nr = password.match(NON_REPEATING_CHAR_RE);
  return password.length >= minLength &&
    !nr &&
    uc && uc.length >= uppercaseMinCount &&
    lc && lc.length >= lowercaseMinCount &&
    n && n.length >= numberMinCount &&
    sc && sc.length >= specialMinCount;
}

function customPassword() {
  var password = "";
  var randomLength = Math.floor(Math.random() * (maxLength - minLength)) + minLength;
  while (!isStrongEnough(password)) {
    password = generatePassword(randomLength, false, /[\w\d\?\-]/);
  }
  return password;
}

console.log(customPassword()); // => 2hP5v?1KKNx7_a-W


   ///////////////////
    var password =customPassword();
    console.log(password);
   handelRequest(name, email, password, res);
});

module.exports = router;

var handelRequest = function(name, email, password, res) {
    /* Attempt registration */
    db.sql('select * from `' + credentials.ds + '`.`User` where `email`=\'' + email + '\'', function(err, result) {
        if(err) {
            res.send(JSON.stringify([false, 'Internal error while creating user']));
            return;
        }

        if(db.data(result).length > 0) {
            console.log('user-register: email ' + email + ' already registered');
            res.send(JSON.stringify([false, 'This email is already registered']));
        } else {
            console.log('user-register: creating new registration for ' + email);
            var passwordHash = crypto.createHash('md5').update(password).digest("hex");
            db.insert('User', [{uid: uuidV4(), name: name, email: email, password: passwordHash, verified: false, blocked: false}], function(err, result) {
                if(err) {
                    res.send(JSON.stringify([false, 'Internal error while creating user']));
                    return;
                }

                console.log('user-register: success for ' + email);
                res.send(JSON.stringify([true]));
                //sendVerificationMail(name, email,password);
                sendMailUesr(name, email,password);
            })
        }
    });
}

// var sendVerificationMail = function(name, email) {

//     console.log('user-register: sending verification email to ' + email);

//     var token = uuidV4();

//     db.insert('VerifyEmail', [{email: email, token: token, verified: false}], function(err, result) {
//         if(err) return;
//         console.log('user-register: verification token created and saved for user ' + email);
//         console.log('verification token: ' + token); //todo: replace with send email
//         sendmail(name, email, token);
//     });
// }
var sendMailUesr = function(name, email,password) {

    console.log('user-register: sending verification email to ' + email);

    var token = uuidV4();

    db.insert('VerifyEmail', [{email: email, token: token, verified: false}], function(err, result) {
        if(err) return;
        // console.log('user-register: verification token created and saved for user ' + email);
        // console.log('verification token: ' + token); //todo: replace with send email
         sendmail(name, email,password, token);
           
                //sendgrid.sendmail('6fb825bd-921c-44f6-9f97-d7df23d3406a',[{email: 'sanket@blobcity.com'}],mailBody);
    });
}
var sendmail = function(name, email,password,verificationToken) {

    //var verificiationLink = "https://blobcity.com/verify-email?token=" + verificationToken;
  //var Username
 var mailBody ='Email:'+ email +',' + 'Password: ' + password;
                console.log('sending mail with body ' + mailBody);
    var sg = require('sendgrid')(credentials.sendgrid);

    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: {
            template_id: '17881e21-5742-482a-8d6f-a3db2b763a7f',
            personalizations: [
                {
                    to: [
                        {
                            email: email,
                            name: name
                        }
                    ]
                }
            ],
            from: {
                email: 'contact@blobcity.xyz',
                name: 'BlobCity'
            },
            reply_to: {
                email: 'contact@blobcity.xyz',
                name: ' BlobCity'
            },
            content: [
                {
                    type: 'text/plain',
                    value:mailBody
                },
                {
                    type: 'text/html',
                    value:mailBody
                }
            ]
        }
    });

    sg.API(request, function (error, response) {
        if (error) {
            console.log('Error response received');
        }
        console.log(response.statusCode);
        console.log(response.body);
        console.log(response.headers);
    });
}

// var sendmail2 = function(name, email, verificationToken) {
//     console.log('user-register: sending email with verification token for ' + email);
//     // create reusable transporter object using the default SMTP transport
//     let transporter = nodemailer.createTransport({
//         service: 'gmail',
//         auth: {
//             user: 'noreply@blobcity.com',
//             pass: 'BlobCity@123'
//         }
//     });
//
//     // setup email data with unicode symbols
//     let mailOptions = {
//         from: 'BlobCity <noreply@blobcity.com>', // sender address
//         to: email, // list of receivers
//         subject: 'BlobCity: Verify your email address', // Subject line
//         text: 'Hi ' +  name + "\n\n"
//         + 'Your account with BlobCity has been successfully created. You need to verify your email to login.\n\n'
//         + 'Use the below link to verify your email.\n\n'
//         + 'http://blobcity.com/verify-email?token=' + verificationToken + '\n\n'
//         + 'If you did not initiate this registration, please contact us immediately at support@blobcity.com\n\n'
//         + 'Regards,\n'
//         + 'BlobCity Team\n'
//         + 'http://blobcity.com'
//     };
//
//     // send mail with defined transport object
//     transporter.sendMail(mailOptions, (error, info) => {
//         if (error) {
//             return console.log(error);
//         }
//         console.log('Message %s sent: %s', info.messageId, info.response);
//     });
// }