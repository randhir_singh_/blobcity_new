/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var credentials = require('../../../bin/credentials');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
    var st = req.query.st;

    handelRequest(st, res);
});

router.post('/', function(req, res, next) {
    var st = req.body.st;

    handelRequest(st, res);
});

module.exports = router;


var handelRequest = function(st, res) {
    db.sql('delete from `' + credentials.ds + '`.`UserSession` where `token`=\'' + st + '\'', function(err, result) {
        //response does not matter.

        res.send(''); //closing the response by sending an empty string
    });
};
