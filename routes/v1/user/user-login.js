/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var uuidV4 = require('uuid.v4');
var db = require('../../../logic/db/blobcity').db();
var crypto = require('crypto');
var credentials = require('../../../bin/credentials');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
    var email = req.query.email;
    var password = req.query.password;

    console.log('login: attempting login for ' + email);

    handelRequest(email, password, res);
});

router.post('/', function(req, res, next) {
    var email = req.body.email;
    var password = req.body.password;

    handelRequest(email, password, res);
});

module.exports = router;


var handelRequest = function(email, password, res) {
    db.sql('select * from `' + credentials.ds + '`.`User` where `email`=\'' + email + '\'', function(err, result) {
        if(err) {
            res.send(JSON.stringify([false]));
            return;
        }

        var data = db.data(result);
        if(data.length <= 0 || data.length > 1) {
            res.send(JSON.stringify([false]));
        } else {
            var savedPasswordHash = data[0].password;
            var enteredPasswordHash = crypto.createHash('md5').update(password).digest("hex");

            if(savedPasswordHash === enteredPasswordHash && data[0].verified && !data[0].blocked) {

                /* Login is successful. Make entry for session token and respond. Old entries have to be auto deleted */
                var token = uuidV4();
                db.insert('UserSession', [{uid: data[0].uid, email: email, token: token, timestamp: Date.now()}], function(err, result) {
                    if(err) {
                        res.send(JSON.stringify([false]));
                        return;
                    }
                    res.send(JSON.stringify([true, token]));
                });
            } else {
                res.send(JSON.stringify([false]));
            }
        }
    });
}
