/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var credentials = require('../../../bin/credentials');
var uuidV4 = require('uuid.v4');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    var st = req.query.st;
    var stripeToken = req.query['stripe-token'];
    var country = req.query.country;
    var pinCode = req.query['pin-code'];

    handelRequest(st, stripeToken, country, pinCode, res);
});

router.post('/', function(req, res, next) {
   var st = req.body.st;
   var stripeToken = req.body.stripe-token;
   var country = req.body.country;
   var pinCode = req.body.pin-code;

   handelRequest(st, stripeToken, country, pinCode, res);
});

module.exports = router;

var handelRequest = function(st, stripeToken, country, pinCode, res) {
    db.sql('select * from `' + credentials.ds + '`.`UserSession` where `token`=\'' + st + '\'', function(err, result) {
        if(err) {
            res.send (JSON.stringify({ack: '0'}));
            return;
        }

        var data = db.data(result);
        if(data.length == 1) {
            var uid = data[0].uid;

            db.sql('select * from `' + credentials.ds + '`.`StripeCards` where `uid`=\'' + uid + '\'', function(err, result) {
                if(err) {
                    res.send(JSON.stringify({ack: '0'}));
                    return;
                }

                if(db.data(result).length > 0) {
                    /* Insert this card as a non-primary card */
                    db.insert('StripeCards', [{id: uuidV4(), uid: uid, token: stripeToken, country: country, zip: pinCode, card: '...xxxx', type: 'AMEX', '_primary': false}], function(err, result) {
                        if(err) {
                            res.send(JSON.stringify({ack: '0'}));
                            return;
                        }

                        console.log('inserted non primary');
                        res.send(JSON.stringify({ack: '1'}));
                    });
                } else {
                    /* Insert this card as a primary card */
                    db.insert('StripeCards', [{id: uuidV4(), uid: uid, token: stripeToken, country: country, zip: pinCode, card: '...xxxx', type: 'AMEX', '_primary': true}], function(err, result) {
                        if(err) {
                            res.send(JSON.stringify({ack: '0'}));
                            return;
                        }

                        console.log('inserted primary');
                        res.send(JSON.stringify({ack: '1'}));
                    });
                }
            });
        } else {
            res.send (JSON.stringify({ack: '0'}));
        }
    });
}