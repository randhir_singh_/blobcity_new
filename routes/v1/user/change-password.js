/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var credentials = require('../../../bin/credentials');
var crypto = require('crypto');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
    var st = req.query.st; //session token
    var oldPassword = req.query.oldPass;
    var newPassword = req.query.newPass;

    handelRequest(st, oldPassword, newPassword, res);
});

router.post('/', function(req, res, next){
    var st = req.body.st; //session token
    var oldPassword = req.body.oldPass;
    var newPassword = req.body.newPass;

    handelRequest(st, oldPassword, newPassword, res);
});

module.exports = router;

var handelRequest = function(st, oldPassword, newPassword, res) {
    db.sql('select * from `' + credentials.ds + '`.`UserSession` where `token`=\'' + st + '\'',  function(err, result) {
        if(err) {
            res.send('failure', 'Internal error, please try after sometime');
            return;
        }

        if(db.data(result).length == 1) {
            var userSession = db.data(result)[0];

            db.sql('select * from `' + credentials.ds + '`.`User` where `email`=\'' + userSession.email + '\'', function(err, result) {
                if(err) {
                    res.send('failure', 'Internal error, please try after sometime');
                    return;
                }

                if(db.data(result).length == 1) {
                    var user = db.data(result)[0];
                    var oldPasswordHash = crypto.createHash('md5').update(oldPassword).digest("hex");
                    var newPasswordHash = crypto.createHash('md5').update(newPassword).digest("hex");

                    if(oldPasswordHash === user.password) {
                        db.sql('update `portal`.`User` set `password`=\'' + newPasswordHash + '\'', function(err, result) {
                            if(err) {
                                res.send ('failure', 'Internal error. Please try again in sometime');
                                return;
                            }

                            res.send ('success');
                        });
                    } else {
                        res.send ('failure', 'Invalid old password');
                    }
                } else {
                    //TODO: Notify admin of the issue
                    res.send('failure', 'Internal error. Please try again in sometime');
                }
            });
        } else {
            res.send ('failure', 'Invalid session token. Please login and try again.');
        }
    });
}
