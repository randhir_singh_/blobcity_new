/**
 * Created by sanketsarang on 08/02/17.
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var credentials = require('../../../bin/credentials');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
    var st = req.query.st;
    var ds = req.query.ds;

    handelRequest(st, ds, res);
});

router.post('/', function(req, res, next) {
    var st = req.body.st;
    var ds = req.body.ds;

    handelRequest(st, ds, res);
});

module.exports = router;


var handelRequest = function(st, ds, res) {

    //TODO: Complete implementation

    var responseJson = {
        ack: '1',
    };

    res.send(JSON.stringify(responseJson));
}
