/**
 * Created by sanketsarang
 */
var express = require('express');
var db = require('../../../logic/db/blobcity').db();
var session = require('../../../logic/session/bc-session');
var apps = require('../../../logic/apps.js');
var Promise = require('bluebird');
var router = express.Router();


Promise.promisifyAll(db);
Promise.promisifyAll(session);
Promise.promisifyAll(apps);



/* GET apps listing. */
router.get('/', function(req, res, next) {
    var st = req.query.st; //session token



    console.log('checking user session now for ' + st);

    session.inSessionAsync(st).then(function(result) {
        // apps.userAppsAsync(uid).then(function(result) {
        //
        // });

        // var dashboards = userApps.then(function(result) {
        //     apps.unsubcribedHostedDashboardsAsync(uid);
        // });
        //
        // var datasets = dashboards.then(function(result) {
        //     apps.unsubscribedHostedDatasetsAsync(uid);
        // });

        // userApps.then(function(result) {
        //    var resultJson = {
        //        'ack' : '1',
        //        'apps' : userApps.value()
        //    };
        //
        //    res.send(JSON.stringify(resultJson));
        // });

        res.send('works');
    }).catch(function(error) {
        res.send(JSON.stringify({'ack': '0', 'cause': 'session invalid'}));
    });


    // session.inSession(st, function(userSession) {
    //     let appsList = await appList(userSession.uid);
    //     var response = {
    //         'ack': '1',
    //         'good': 'job',
    //         'apps': appsList,
    //         'datasets': apps.unsubscribedHostedDatasets(userSession.uid),
    //         'dashboards': apps.unsubcribedHostedDashboards(userSession.uid)
    //     };
    //
    //     console.log('sending response ' + JSON.stringify(response));
    //     res.send(JSON.stringify(response));
    // });

    // res.send(JSON.stringify({'ack': '0', 'cause': 'session invalid'}));
});


var getUserAppsSection = function(uid) {
    return getUserApps();
}



var getUserApps = function(uid) {
    db.sql('select * from `portal`.`UserApps` where `uid`=\'' + uid + '\'', function(result){
        if(db.isSuccess(result)) {
            return db.data(result);
        }
    });

    return [];
}

var getAllHostedDashboards = function() {
    // db.sql('select * from `portal`.`Apps` where `type`=`DASHBOARD`', function(result){
    //     if(db.isSuccess(result)) {
    //         return db.data(result);
    //     }
    // });

    return [];
}

var getAllHostedDatasets = function() {
    // db.sql('select * from `portal`.`Apps` where `type`=`DATASET`', function(result){
    //     if(db.isSuccess(result)) {
    //         return db.data(result);
    //     }
    // });

    return [];
}

module.exports = router;
